go build -o .\dockerfiles\build\gitlab-runner-helper.exe .\apps\gitlab-runner-helper\main.go
if (-not $?) {
    Write-Error 'An error occurred during gitlab-runner-helper build.'
    exit($LASTEXITCODE)
}

$revision = git rev-parse --short HEAD
docker build -t gitlab/gitlab-runner-helper:win-x86_64-$($REVISION) -f dockerfiles/build/Dockerfile.windows.x86_64 dockerfiles/build